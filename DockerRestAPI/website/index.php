<html>
<head>
    <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js">
    </script>
</head>

<body>
    <div>
        <label for="top"> Grab the top k brevets: </label>
        <input id="top" type="number">
    <div>
        <button name="show" onclick="fetch('', '/json')"> List All Default</button>
        <button name="show" onclick="fetch('', '/json')"> List All JSON</button>
        <button name="show" onclick="fetch('', '/csv')"> List All CSV</button>
    </div>
    <div>
        <button name="show" onclick="fetch('/listOpenOnly', '/json')"> List Open Only Default</button>
        <button name="show" onclick="fetch('/listOpenOnly', '/json')"> List Open Only JSON</button>
        <button name="show" onclick="fetch('/listOpenOnly', '/csv')"> List Open Only CSV</button>
    </div>
    <div>
        <button name="show" onclick="fetch('/listCloseOnly', '/json')"> List Close Only Default</button>
        <button name="show" onclick="fetch('/listCloseOnly', '/json')"> List Close Only JSON</button>
        <button name="show" onclick="fetch('/listCloseOnly', '/csv')"> List Close Only CSV</button>
    </div>
        <ul id="brevets"></ul>
        <div id="csvspace"></div>
    </div>
    <script>
    var listelement = document.getElementById('brevets');

    function fetch(openclose, format) {
        while (listelement.firstChild) {
            listelement.removeChild(listelement.firstChild)
        }
        while (csvspace.firstChild) {
            csvspace.removeChild(csvspace.firstChild)
        }

        if (openclose == "")
            openclose = "/listAll"

        request = window.location.origin.split(':').splice(0, 2, 1).join(':') + ':' + '5021';
        request += openclose + format;
        var top = $("#top").val();

        if (format == "/json") {
            $.getJSON(request, {top: top},
            function(data) {
                for(brevet of data.Brevetlist) {
                    var entry = document.createElement('h1');
                    entry.appendChild(document.createTextNode('Brevet'));
                    listelement.appendChild(entry);

                    var entry = document.createElement('li');
                    entry.appendChild(document.createTextNode('Distance: ' + brevet.distance));
                    listelement.appendChild(entry);

                    var entry = document.createElement('li');
                    entry.appendChild(document.createTextNode("Begin Date: " + brevet.begin_date));
                    listelement.appendChild(entry);

                    var entry = document.createElement('li');
                    entry.appendChild(document.createTextNode("Begin Time: " + brevet.begin_time));
                    listelement.appendChild(entry);
                    var i = 1;
                    for (control of brevet.controls) {
                        var entry = document.createElement('li');
                        entry.appendChild(document.createTextNode("Control: " + i));
                        entry.appendChild(document.createElement('br'));
                        for (prop in control) {
                            details = prop + ": " + control[prop];
                            entry.appendChild(document.createTextNode(details));
                            entry.appendChild(document.createElement('br'));
                        }
                        listelement.appendChild(entry);
                        i++;
                    }
                }
            });
        } else {
            while (csvspace.firstChild) {
                csvspace.removeChild(csvspace.firstChild)
            }
            while (listelement.firstChild) {
                listelement.removeChild(listelement.firstChild)
            }
            $.get(request, {top: top},
            function(data) {
                let csvspace = $("<div'></div>");
                csvspace.html(JSON.stringify(data))
                $('#csvspace').append(csvspace);
            });
        }
    }
    </script>
</body>
</html>