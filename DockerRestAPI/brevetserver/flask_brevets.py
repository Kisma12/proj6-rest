"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient('db', 27017)
db = client.tododb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_dist = request.args.get('brevet_dist', type=float)
    sd = request.args.get('startDate')
    st = request.args.get('startTime')
    startTime = arrow.get(sd + " " + st, "YYYY-MM-DD HH:mm", tzinfo='US/Pacific').isoformat()
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, brevet_dist, startTime)
    close_time = acp_times.close_time(km, brevet_dist, startTime)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route("/_submit", methods=['POST', 'GET'])
def submit():
    i = 0
    controls = []
    errmsg = ""
    empty = True
    prevctrl = -1

    while (request.args.get('controls[' + str(i) + '][miles]') != None):
        km = request.args.get('controls[' + str(i) + '][km]')
        miles = request.args.get('controls[' + str(i) + '][miles]')
        location = request.args.get('controls[' + str(i) + '][location]')
        opentime = request.args.get('controls[' + str(i) + '][opentime]')
        closetime = request.args.get('controls[' + str(i) + '][closetime]')
        i += 1
        if (km == 'NaN'):
            app.logger.debug("Km cannot be NaN.")
            errmsg = "Km cannot be NaN."
        elif (float(km) > float(request.args.get('maxdist'))):
            app.logger.debug("Control point placed too far past maximum brevet length.")
            errmsg = "Control point placed too far past maximum brevet length."
        elif (prevctrl > float(km)):
            app.logger.debug("Controls must be entered in ascending order based on km.")
            errmsg = "Controls must be entered in ascending order based on km."
        else:
            empty = False
            controls.append({
                "km": km,
                "miles": miles,
                "location": location,
                "opentime": opentime,
                "closetime": closetime,
            })

    if (empty):
        app.logger.debug("You must enter at least 1 control point for your brevet.")
        errmsg = "You must enter at least 1 control point for your brevet."

    if (errmsg == ""):
        brevet_doc = {
            'distance': request.args.get('distance'),
            'begin_date': request.args.get('begin_date'),
            'begin_time': request.args.get('begin_time'),
            'controls': controls # Desired: List of dicts of key:value pairs
        }
        db.tododb.insert_one(brevet_doc)
        return redirect(url_for('index'))
    
    else:
        item = {'errmsg': errmsg}
        return render_template('errorPage.html', item=item)

@app.route("/display", methods=['GET'])
def display():
    if (db.tododb.count() == 0):
        item = {'errmsg': "Database is empty. Nothing to display."}
        return render_template('errorPage.html', item=item)
    _brevets = db.tododb.find()
    brevets = [brevet for brevet in _brevets]
    return render_template('brevets.html', brevets=brevets)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
