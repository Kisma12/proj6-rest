import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route('/')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    table = []
    testData = request.form.getlist('testData')
    testSection = request.form.getlist('testSection')

    for index, control in enumerate(testData):
        if(testData[index] != ""):
            table.append(testData[index] + ", " + testSection[index])

    item_doc = {
        'name': request.form['name'],
        'description': request.form['description'],
        'km': request.form['km'],
        'miles': request.form['miles'],
        'table': table,
        'testData': request.form.getlist('testData')
    }
    db.tododb.insert_one(item_doc)

    return redirect(url_for('todo'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
