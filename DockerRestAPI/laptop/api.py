# User Service

import flask
import pymongo
import csv
from pymongo import MongoClient
from flask import Flask, request
from flask_restful import Resource, Api
import pandas


# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient('db', 27017)
db = client.tododb

class ListAllJson(Resource):
    def get(self):
        top = request.args.get('top')
        _brevets = db.tododb.find()
        brevets = [brevet for brevet in _brevets]

        if (top != ""):
            app.logger.debug(f"Trimming brevets to {int(top)}")
            brevets = brevets[0:int(top)] # trims brevets to the top k brevets

        for brevet in brevets:
            brevet.pop('_id', None) # gets rid of _id appended by MongoDB
        
        responseObj = {
            "Brevetlist": brevets
        }
        response = flask.make_response(flask.jsonify(responseObj))
        response.headers['Access-Control-Allow-Origin'] = '*'
        return response

class ListOpenOnlyJson(Resource):
    def get(self):
        top = request.args.get('top')
        _brevets = db.tododb.find()
        brevets = [brevet for brevet in _brevets]
        
        if (top != ""):
            app.logger.debug(f"Trimming brevets to {int(top)}")
            brevets = brevets[0:int(top)] # trims brevets to the top k brevets

        for brevet in brevets:
            brevet.pop('_id', None) # gets rid of _id appended by MongoDB
            for control in brevet['controls']:
                control.pop('closetime', None) # eliminates the closetime field of a control

        responseObj = {
            "Brevetlist": brevets
        }
        response = flask.make_response(flask.jsonify(responseObj))
        response.headers['Access-Control-Allow-Origin'] = '*'
        return response

class ListCloseOnlyJson(Resource):
    def get(self):
        top = request.args.get('top')
        _brevets = db.tododb.find()
        brevets = [brevet for brevet in _brevets]

        if (top != ""):
            app.logger.debug(f"Trimming brevets to {int(top)}")
            brevets = brevets[0:int(top)] # trims brevets to the top k brevets
        
        for brevet in brevets:
            brevet.pop('_id', None) # gets rid of _id appended by MongoDB
            for control in brevet['controls']:
                control.pop('opentime', None) # eliminates the opentime field of a control
        
        responseObj = {
            "Brevetlist": brevets
        }
        response = flask.make_response(flask.jsonify(responseObj))
        response.headers['Access-Control-Allow-Origin'] = '*'
        return response

class ListAllCsv(Resource):
    def get(self):
        top = request.args.get('top')
        _brevets = db.tododb.find()
        brevets = [brevet for brevet in _brevets]
        csvfile = ""

        if (top != ""):
            app.logger.debug(f"Trimming brevets to {int(top)}")
            brevets = brevets[0:int(top)] # trims brevets to the top k brevets
        
        for brevet in brevets:
            brevet.pop('_id', None) # gets rid of _id appended by MongoDB
            datafr = pandas.DataFrame.from_dict(brevet)
            csvfile += datafr.to_csv()
        response = flask.make_response(csvfile)
        response.headers['Access-Control-Allow-Origin'] = '*'
        return response

class ListOpenOnlyCsv(Resource):
    def get(self):
        top = request.args.get('top')
        _brevets = db.tododb.find()
        brevets = [brevet for brevet in _brevets]
        csvfile = ""

        if (top != ""):
            app.logger.debug(f"Trimming brevets to {int(top)}")
            brevets = brevets[0:int(top)] # trims brevets to the top k brevets
        
        for brevet in brevets:
            brevet.pop('_id', None) # gets rid of _id appended by MongoDB
            for control in brevet['controls']:
                control.pop('closetime', None) # eliminates the opentime field of a control
            datafr = pandas.DataFrame.from_dict(brevet)
            csvfile += datafr.to_csv()
        response = flask.make_response(csvfile)
        response.headers['Access-Control-Allow-Origin'] = '*'
        return response

class ListCloseOnlyCsv(Resource):
    def get(self):
        top = request.args.get('top')
        _brevets = db.tododb.find()
        brevets = [brevet for brevet in _brevets]
        csvfile = ""

        if (top != ""):
            app.logger.debug(f"Trimming brevets to {int(top)}")
            brevets = brevets[0:int(top)] # trims brevets to the top k brevets
        
        for brevet in brevets:
            brevet.pop('_id', None) # gets rid of _id appended by MongoDB
            for control in brevet['controls']:
                control.pop('opentime', None) # eliminates the opentime field of a control
            datafr = pandas.DataFrame.from_dict(brevet)
            csvfile += datafr.to_csv()
        response = flask.make_response(csvfile)
        response.headers['Access-Control-Allow-Origin'] = '*'
        return response

# Create routes
# Another way, without decorators
api.add_resource(ListAllJson, '/', '/listAll', '/listAll/json')
api.add_resource(ListAllCsv, '/listAll/csv')
api.add_resource(ListOpenOnlyJson, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(ListOpenOnlyCsv, '/listOpenOnly/csv')
api.add_resource(ListCloseOnlyJson, '/listCloseOnly', '/listCloseOnly/json')
api.add_resource(ListCloseOnlyCsv, '/listCloseOnly/csv')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001, debug=True)
